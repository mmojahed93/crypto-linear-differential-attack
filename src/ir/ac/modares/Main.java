package ir.ac.modares;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by noname on 1/24/19.
 */
public class Main {

    private final static int ARRAY_LENGTH = 16;

    private final static int sbox_number = 4;

    private final static int[] testSox = {14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7};
    private final static int[] sSox1 = {7, 0, 9, 5, 12, 6, 10, 3, 8, 11, 15, 2, 1, 13, 4, 14};
    private final static int[] sSox2 = {8, 3, 5, 9, 11, 12, 6, 10, 1, 13, 2, 14, 4, 7, 15, 0};
    private final static int[] sSox3 = {0, 5, 10, 3, 7, 9, 12, 15, 11, 2, 6, 13, 8, 14, 1, 4};
    private final static int[] sSox4 = {13, 6, 0, 10, 14, 3, 11, 5, 7, 1, 9, 4, 2, 8, 12, 15};


    private final static int[][] sBoxes = {sSox1, sSox2, sSox3, sSox4};
    private final static int[][][] linearAttackRelations = new int[sbox_number][ARRAY_LENGTH][ARRAY_LENGTH];
    private final static int[][][] differentialAttackRelations = new int[sbox_number][ARRAY_LENGTH][ARRAY_LENGTH];

    public static void main(String[] args) {

        final long startTime = System.currentTimeMillis();

        /////////////////////////////////////
        // LINEAR ATTACK
        /////////////////////////////////////

        //
        // Generate linear relations
        //
        for (int sboxIndex = 0; sboxIndex < sbox_number; sboxIndex++) {
            linearAttackRelations[sboxIndex] = calcLinearRelation(sBoxes[sboxIndex]);
        }

        //
        // Find best linear attack Section A
        //
        System.out.println("\n");
        System.out.println("############ Linear Attack, Section A Best Result ############");
        MaxResult[] linearAttackResultA = findBestAttack(linearAttackRelations, true);
        for (MaxResult maxResult : linearAttackResultA) {

            System.out.println("Linear=> Section A Best Bios: " + maxResult.getTotalBios());
            System.out.println(maxResult.getPath());

        }

        //
        // Find best linear attack Section B
        //
        System.out.println("\n");
        System.out.println("############ Linear Attack, Section B Best Result ############");
        MaxResult[] linearAttackResultB = findBestAttack(linearAttackRelations, false);
        for (MaxResult maxResult : linearAttackResultB) {

            System.out.println("Linear=> Section B Best Bios: " + maxResult.getTotalBios());
            System.out.println(maxResult.getPath());

        }

        //
        // Check linear relation table
        //
//        System.out.println("#################### Linear Attack Relation Table ####################");
//        for (int s = 0; s < sbox_number; s++) {
//            for (int i = 0; i < ARRAY_LENGTH; i++) {
//                String output = "";
//                for (int j = 0; j < ARRAY_LENGTH; j++) {
//                    output += linearAttackRelations[s][i][j] + ", ";
//                }
//                System.out.println(output);
//            }
//            System.out.println("\n\n");
//        }


        /////////////////////////////////////
        // DIFFERENTIAL ATTACK
        /////////////////////////////////////

        //
        // Generate differential relations
        //
        for (int sboxIndex = 0; sboxIndex < sbox_number; sboxIndex++) {
            differentialAttackRelations[sboxIndex] = calcDifferentialRelation(sBoxes[sboxIndex]);
        }

        //
        // Find best differential attack Section A
        //
        System.out.println("\n");
        System.out.println("############ Differential Attack, Section A Best Result ############");
        MaxResult[] differentialAttackResultA = findBestAttack(differentialAttackRelations, true);
        for (MaxResult maxResult : differentialAttackResultA) {

            System.out.println("Differential=> Section A Best Probability: " + maxResult.getTotalBios());
            System.out.println(maxResult.getPath());
        }

        //
        // Find best differential attack Section B
        //
        System.out.println("\n");
        System.out.println("############ Differential Attack, Section B Best Result ############");
        MaxResult[] differentialAttackResultB = findBestAttack(differentialAttackRelations, false);
        for (MaxResult maxResult : differentialAttackResultB) {

            System.out.println("Differential=> Section B Best Probability: " + maxResult.getTotalBios());
            System.out.println(maxResult.getPath());

        }

        //
        // Check differential relation table
        //
//        System.out.println("#################### Differential Attack Relation Table ####################");
//        for (int s = 0; s < sbox_number; s++) {
//            for (int i = 0; i < ARRAY_LENGTH; i++) {
//                String output = "";
//                for (int j = 0; j < ARRAY_LENGTH; j++) {
//                    output += differentialAttackRelations[s][i][j] + ", ";
//                }
//                System.out.println(output);
//            }
//            System.out.println("\n\n");
//        }


        final long endTime = System.currentTimeMillis();
        final float runTimeDuration = (float) (endTime - startTime) / 1000;
        System.out.println("\n");
        System.out.println("Total Run Duration: " + runTimeDuration + "'s");

    }

    /**
     * @param relationTable
     * @param isQuestionSectionA
     * @return
     */

    private static MaxResult[] findBestAttack(int[][][] relationTable, boolean isQuestionSectionA) {
        MaxResult[] topFiveResult = new MaxResult[5];
        for (int i = 0; i < topFiveResult.length; i++) {
            topFiveResult[i] = new MaxResult(0, new ArrayList<>());
        }
        float secondLayerMaxMultipleBios;
        float thirdLayerMaxMultipleBios;

        final ArrayList<Sbox> firstLayerSboxes = new ArrayList<>();
        final ArrayList<Sbox> secondLayerSboxes = new ArrayList<>();
        final ArrayList<Sbox> thirdLayerSboxes = new ArrayList<>();
        final ArrayList<Sbox> finalTotalSboxes = new ArrayList<>();

        //
        // FIRST LAYER
        //
        // input of each 4 box is 1,...,15
        for (int firstLayerInput = 1; firstLayerInput < 16; firstLayerInput++) { // inputs (plain text)

            float firstLayerMaxBios2 = 0;
            for (int firstLayerSboxIndex = 0; firstLayerSboxIndex < sbox_number; firstLayerSboxIndex++) { // select sbox

                int[] firstLayerAiRow = relationTable[isQuestionSectionA ? 0 : firstLayerSboxIndex][firstLayerInput];
                // for each item in corresponding relation row of this sbox
                float firstLayerMaxBios = 0;
                for (int firstLayerSboxOutput = 0; firstLayerSboxOutput < firstLayerAiRow.length; firstLayerSboxOutput++) {
                    float firstLayerSboxBios = (float) firstLayerAiRow[firstLayerSboxOutput] / 16;
                    if (firstLayerSboxBios == 0) {
                        continue;
                    }

                    // find next layer inputs (which sbox and input of them)
                    int firstLayer_ith_bit_of_next_layer = sbox_number - firstLayerSboxIndex; // for example: 3th bit -> 4 should be as input for next layer (2 pow (3-1))
                    int firstLayer_ith_sbox_of_next_layer[] = new int[sbox_number]; // find which sbox of next layer is active
                    for (int sboxIndex = 0; sboxIndex < sbox_number; sboxIndex++) { // From top to bottom, Correct order of sbox
                        firstLayer_ith_sbox_of_next_layer[sbox_number - sboxIndex - 1] = firstLayerSboxOutput & ((int) Math.pow(2, sboxIndex));
                    }

                    //
                    // SECOND LAYER
                    //
                    secondLayerMaxMultipleBios = 1;
                    secondLayerSboxes.clear();
                    for (int secondLayerSboxIndex = 0; secondLayerSboxIndex < firstLayer_ith_sbox_of_next_layer.length; secondLayerSboxIndex++) {
                        if (firstLayer_ith_sbox_of_next_layer[secondLayerSboxIndex] == 0) {
                            continue;
                        }

                        int secondLayerInput = (int) Math.pow(2, firstLayer_ith_bit_of_next_layer - 1);
                        int[] secondLayerAiRow = relationTable[isQuestionSectionA ? 1 : secondLayerSboxIndex][secondLayerInput];
                        // for each item in corresponding relation row of this sbox
                        float secondLayerMaxBios = 0;
                        final ArrayList<Sbox> secondLayerSBoxesTemp = new ArrayList<>();
                        for (int secondLayerSboxOutput = 0; secondLayerSboxOutput < secondLayerAiRow.length; secondLayerSboxOutput++) {
                            float secondLayerSboxBios = (float) secondLayerAiRow[secondLayerSboxOutput] / 16;
                            if (secondLayerSboxBios == 0) {
                                continue;
                            }

                            // find next layer inputs (which sbox and input of them)
                            int secondLayer_ith_bit_of_next_layer = sbox_number - secondLayerSboxIndex; // for example: 3th bit -> 4 should be as input for next layer (2 pow (3-1))
                            int secondLayer_ith_sbox_of_next_layer[] = new int[sbox_number]; // find which sbox of next layer is active
                            for (int sboxIndex = 0; sboxIndex < sbox_number; sboxIndex++) { // From top to bottom, Correct order of sbox
                                secondLayer_ith_sbox_of_next_layer[sbox_number - sboxIndex - 1] = secondLayerSboxOutput & ((int) Math.pow(2, sboxIndex));
                            }

                            //
                            // THIRD LAYER
                            //
                            thirdLayerMaxMultipleBios = 1;
                            thirdLayerSboxes.clear();
                            for (int thirdLayerSboxIndex = 0; thirdLayerSboxIndex < secondLayer_ith_sbox_of_next_layer.length; thirdLayerSboxIndex++) {
                                if (secondLayer_ith_sbox_of_next_layer[thirdLayerSboxIndex] == 0) {
                                    continue;
                                }

                                int thirdLayerInput = (int) Math.pow(2, secondLayer_ith_bit_of_next_layer - 1);
                                int[] thirdLayerAiRow = relationTable[isQuestionSectionA ? 2 : thirdLayerSboxIndex][thirdLayerInput];

                                // select best bios of current sbox in third layer
                                int lastOutput = -1;
                                float thirdLayerMaxBios = 0;
                                for (int thirdLayerSboxOutput = 0; thirdLayerSboxOutput < thirdLayerAiRow.length; thirdLayerSboxOutput++) {
                                    float thirdLayerSboxBios = (float) thirdLayerAiRow[thirdLayerSboxOutput] / 16;
                                    if (Math.abs(thirdLayerSboxBios) > Math.abs(thirdLayerMaxBios)) {
                                        thirdLayerMaxBios = thirdLayerSboxBios;
                                        lastOutput = thirdLayerSboxOutput;
                                    }
                                }

                                thirdLayerSboxes.add(new Sbox(thirdLayerInput, lastOutput, thirdLayerSboxIndex, 3, thirdLayerMaxBios));
                                thirdLayerMaxMultipleBios *= thirdLayerMaxBios;

                            } // end of third layer loop

//                            thirdLayerMaxMultipleBios *= secondLayerSboxBios;
                            if (Math.abs(thirdLayerMaxMultipleBios * secondLayerSboxBios) > Math.abs(secondLayerMaxBios)) {
                                secondLayerMaxBios = thirdLayerMaxMultipleBios * secondLayerSboxBios;

                                secondLayerSBoxesTemp.clear();
                                secondLayerSBoxesTemp.addAll(thirdLayerSboxes);
                                secondLayerSBoxesTemp.add(new Sbox(secondLayerInput, secondLayerSboxOutput, secondLayerSboxIndex, 2, secondLayerSboxBios));

                            }

                        }

                        secondLayerSboxes.addAll(secondLayerSBoxesTemp);
                        secondLayerMaxMultipleBios *= secondLayerMaxBios;

                    } // end of second layer loop

//                    secondLayerMaxMultipleBios *= firstLayerSboxBios;
                    if (Math.abs(secondLayerMaxMultipleBios * firstLayerSboxBios) > Math.abs(firstLayerMaxBios)) {
                        firstLayerMaxBios = secondLayerMaxMultipleBios * firstLayerSboxBios;

                        firstLayerSboxes.clear();
                        firstLayerSboxes.addAll(secondLayerSboxes);
                        firstLayerSboxes.add(new Sbox(firstLayerInput, firstLayerSboxOutput, firstLayerSboxIndex, 1, firstLayerSboxBios));

                    }

                }

                if (Math.abs(firstLayerMaxBios) > Math.abs(firstLayerMaxBios2)) {
                    firstLayerMaxBios2 = firstLayerMaxBios;
                    finalTotalSboxes.clear();
                    finalTotalSboxes.addAll(firstLayerSboxes);
                }

            } // end of first layer loop

            topFiveResult = sortIgnoreSign(topFiveResult, true);
            if (Math.abs(firstLayerMaxBios2) > Math.abs(topFiveResult[0].getValue())) {
                topFiveResult[0] = new MaxResult(firstLayerMaxBios2, new ArrayList<>(finalTotalSboxes));
            }

        }

        topFiveResult = sortIgnoreSign(topFiveResult, false);
        return topFiveResult;
    }

    private static MaxResult[] sortIgnoreSign(MaxResult[] arr, boolean asc) {
        ArrayList<MaxResult> fixed = new ArrayList<>(Arrays.asList(arr));
        if (asc) {
            fixed.sort((a, b) -> Math.abs(a.getValue()) < Math.abs(b.getValue()) ? -1 : Math.abs(a.getValue()) == Math.abs(b.getValue()) ? 0 : 1);
        } else {
            fixed.sort((a, b) -> Math.abs(a.getValue()) < Math.abs(b.getValue()) ? 1 : Math.abs(a.getValue()) == Math.abs(b.getValue()) ? 0 : -1);
        }

        return fixed.toArray(new MaxResult[arr.length]);
    }

    private static int[][] calcLinearRelation(int[] sbox) {
        final int[][] relation = new int[ARRAY_LENGTH][ARRAY_LENGTH];

        for (int i = 0; i < ARRAY_LENGTH; i++) { // generate ai states

            int a_states = i;
            int[] bios_list = new int[ARRAY_LENGTH];
            for (int j = 0; j < ARRAY_LENGTH; j++) { // generate bi states
                int b_states = j;

                int number_of_match = 0;
                for (int k = 0; k < ARRAY_LENGTH; k++) {
                    int and_of_a_states = a_states & k;
                    int and_of_b_states = b_states & sbox[k];

                    int a_xor = 0;
                    int b_xor = 0;
                    for (int l = 3; l > -1; l--) {
                        int a_bit = (and_of_a_states >> l) & 1;
                        a_xor ^= a_bit;

                        int b_bit = (and_of_b_states >> l) & 1;
                        b_xor ^= b_bit;
                    }

                    if (a_xor == b_xor) {
                        number_of_match++;
                    }

                }

                // Calculate bios
                float probability = (float) number_of_match / ARRAY_LENGTH;
                float bios = (float) (probability - 0.5);
                int bios_face = (int) (bios * ARRAY_LENGTH);
                bios_list[j] = bios_face;
            }

            relation[i] = bios_list;
        }

        relation[0][0] = 8;

        return relation;
    }

    private static int[][] calcDifferentialRelation(int[] sbox) {
        final int[][] relation = new int[ARRAY_LENGTH][ARRAY_LENGTH];

        for (int dx = 0; dx < ARRAY_LENGTH; dx++) { // generate dx states

            int[] bios_list = new int[ARRAY_LENGTH];
            for (int dy = 0; dy < ARRAY_LENGTH; dy++) { // generate dy states

                int number_of_match = 0;
                for (int x = 0; x < ARRAY_LENGTH; x++) { // generate x

                    int y = sbox[x];
                    int xp = dx ^ x;
                    int yp = sbox[xp];

                    int current_dy = y ^ yp;
                    if (current_dy == dy) {
                        number_of_match++;
                    }

                }

                // Set prob
                bios_list[dy] = number_of_match;
            }

            relation[dx] = bios_list;
        }

        relation[0][0] = 16;

        return relation;
    }

    public static class MaxResult {
        private float value;
        private ArrayList<Sbox> sboxes;

        public MaxResult(float value, ArrayList<Sbox> sboxes) {
            this.value = value;
            this.sboxes = sboxes;
        }

        public float getValue() {
            return value;
        }

        public void setValue(float value) {
            this.value = value;
        }

        public ArrayList<Sbox> getSboxes() {
            return sboxes;
        }

        public void setSboxes(ArrayList<Sbox> sboxes) {
            this.sboxes = sboxes;
        }

        public double getTotalBios() {
            double maxBios = 1;
            for (Sbox sbox : getSboxes()) {
                maxBios *= sbox.getBios();
            }

            return maxBios;
        }

        public String getPath() {
            String resultMessage = "Path=>\n";
            for (int i = getSboxes().size() - 1; i > -1; i--) {

                Sbox sbox = getSboxes().get(i);
                resultMessage += "Layer: " + sbox.getLayer() + " index: " + sbox.getIndex()
                        + " in: " + sbox.getIn() + " out: " + sbox.getOut() + " bios: " + sbox.getBios() + "\n";
            }

            return resultMessage;
        }
    }

    public static class Sbox {
        private int in;
        private int out;
        private int index;
        private int layer;
        private float bios;

        public Sbox(int in, int out, int index, int layer, float bios) {
            this.in = in;
            this.out = out;
            this.index = index;
            this.layer = layer;
            this.bios = bios;
        }

        public int getIn() {
            return in;
        }

        public void setIn(int in) {
            this.in = in;
        }

        public int getOut() {
            return out;
        }

        public void setOut(int out) {
            this.out = out;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public int getLayer() {
            return layer;
        }

        public void setLayer(int layer) {
            this.layer = layer;
        }

        public float getBios() {
            return bios;
        }

        public void setBios(float bios) {
            this.bios = bios;
        }
    }

}
